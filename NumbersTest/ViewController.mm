//
//  ViewController.m
//  NumbersTest
//
//  Created by Artem Nikiforov on 09.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#import "ViewController.h"
#import <string>
#import <stdexcept>
#import "Number2Words/NumbersConvertionManager.hpp"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void) startConverting
{
    [self.inputView resignFirstResponder];
    
    NSString *inputString = self.inputView.text;
    
    std::string cppString([inputString cStringUsingEncoding:NSUTF8StringEncoding]);
    try {
        auto convertionResult = NumbersConvertionManager::getInstance()->convertToWords(cppString, NumbersConvertionManager::Locale::EN_Gb);
        self.outputView.text = [NSString stringWithCString: convertionResult.c_str()
                                                  encoding: NSUTF8StringEncoding];
    } catch (std::exception &excp) {
        self.outputView.text = [NSString stringWithCString: excp.what()
                                                  encoding: NSUTF8StringEncoding];
    }
}

- (IBAction)onConvertTouch:(UIButton *)sender
{
    [self startConverting];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self startConverting];
    
    return YES;
}

@end
