//
//  AppDelegate.h
//  NumbersTest
//
//  Created by Artem Nikiforov on 09.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

