//
//  main.m
//  NumbersTest
//
//  Created by Artem Nikiforov on 09.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
