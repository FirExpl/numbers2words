//
//  ViewController.h
//  NumbersTest
//
//  Created by Artem Nikiforov on 09.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property IBOutlet UITextField* inputView;
@property IBOutlet UITextView* outputView;

- (IBAction) onConvertTouch: (UIButton*) sender;

@end

