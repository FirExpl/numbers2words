//
//  NumbersConvertionManager.cpp
//  Number2Words
//
//  Created by Artem Nikiforov on 06.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#include "NumbersConvertionManager.hpp"
#include "EnglishNaturalConverter.hpp"

NumbersConvertionManager* NumbersConvertionManager::s_instance = nullptr;

NumbersConvertionManager* NumbersConvertionManager::getInstance()
{
    if(!s_instance)
    {
        s_instance = new NumbersConvertionManager;
    }
    
    return s_instance;
}

std::string NumbersConvertionManager::convertToWords(const std::string &pNumberAsString, NumbersConvertionManager::Locale locale)
{
    switch (locale) {
        case Locale::EN_Gb:
        default:
            EnglishNaturalConverter converter;
            return converter.convert(pNumberAsString);
    }
}

NumbersConvertionManager::NumbersConvertionManager()
{
    
}