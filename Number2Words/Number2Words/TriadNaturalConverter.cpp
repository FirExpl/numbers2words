//
//  TriadNaturalConverter.cpp
//  Number2Words
//
//  Created by Artem Nikiforov on 07.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#include "TriadNaturalConverter.hpp"
#include <stdexcept>
#include <vector>
#include <sstream>

TriadNaturalConverter::~TriadNaturalConverter()
{
    
}

void TriadNaturalConverter::validateInput(const std::string &pNumberAsString)
{
    if(pNumberAsString.empty())
    {
        throw std::invalid_argument("Empty input");
    }
    
    for(auto symbol: pNumberAsString)
    {
        if(!std::isdigit(symbol))
        {
            throw std::invalid_argument(std::string("Wrong symbol in input: ") + symbol);
        }
    }
    
    if(pNumberAsString.length() > this->getMaxNumberLength())
    {
        throw std::invalid_argument("Number is too long");
    }
}

std::string TriadNaturalConverter::convert(const std::string &pNumberStringRepresentation)
{
    this->validateInput(pNumberStringRepresentation);
    
    std::string result = this->simpleConvertion(pNumberStringRepresentation);
    if( result.empty() ) //if simple convertion didn't happen
    {
        auto triadsArray = this->convertToTriads(pNumberStringRepresentation);
        result = this->joinTriadsAsWords(triadsArray);
    }
    
    return result;
}

std::string TriadNaturalConverter::triadToWords(const Triad pTriad, int pTriadPower)
{
    if(pTriad >= 1000)//if number has length more then 3 it isn't triad
    {
        throw std::invalid_argument(std::string("Wrong triad parameter: ") + std::to_string(pTriad));
    }
    
    if(pTriad == 0)//in this case we must skip this triad
    {
        return "";
    }
    
    auto kOnes = this->getOnesNames(pTriad, pTriadPower);
    
    if(pTriad < kOnes.size())//we already have right name for triad
    {
        return kOnes[pTriad];
    }
    
    auto kHundredName = this->getPowerName(0, pTriad);
    
    std::stringstream ss;

    if(kHundredName.length() > 0)
    {
        ss << kHundredName;
    }
    
    int tensNumber = pTriad % 100;
    if(tensNumber > 0)
    {
        auto kSeparators = this->getTriadInnerSeparators();
        
        if(kHundredName.length() > 0)//we need separator only in case when we have hundred name
        {
            ss << kSeparators[0];
        }
        
        if(tensNumber < kOnes.size())
        {
            ss << kOnes[tensNumber];
        }
        else
        {
            auto kTens = this->getTensNames();
            
            int tensDigit = tensNumber / 10;
            ss << kTens[tensDigit -2]; //kTens contains only 8 values, but we have 10 digits, but in this case "tensDigit" greater than 1
            
            int onesDigit = tensNumber % 10;
            if(onesDigit > 0)
            {
                ss << kSeparators[1] << kOnes[onesDigit];
            }
        }
    }
    
    return ss.str();
}

std::vector<TriadNaturalConverter::Triad> TriadNaturalConverter::convertToTriads(const std::string &pNumberAsString)
{
    auto numberLength = pNumberAsString.length();
    int shortTriadLength = numberLength % TRIAD_LENGTH; //the leftest triad can be shorter than TRIAD_LENGTH
    const size_t elementsCount = numberLength / TRIAD_LENGTH + ( ( shortTriadLength != 0) ? 1 : 0);
    std::vector<Triad> result(elementsCount);
    
    int i = 0;
    if(shortTriadLength > 0)
    {
        result[i] = std::stoi(pNumberAsString.substr(0, shortTriadLength));
        ++i;
    }
    
    for(int pos = shortTriadLength; pos < numberLength; pos += TRIAD_LENGTH, ++i)
    {
        result[i] = std::stoi(pNumberAsString.substr(pos, TRIAD_LENGTH));
    }
    
    return result;
}

std::string TriadNaturalConverter::joinTriadsAsWords(const std::vector<Triad> &pTriadsArray)
{
    auto separators = this->getTriadsSeparators();
    
    auto triadsCount = pTriadsArray.size();
    std::stringstream resultStream;
    for(unsigned int i = 0; i < triadsCount; ++i)
    {
        auto triad = pTriadsArray[i];
        
        int triadPower = triadsCount - (i+1);
        auto triadWords = this->triadToWords(triad, triadPower);
        if(triadWords.length() > 0)
        {
            if(i != triadsCount -1)
            {
                auto powerName = this->getPowerName(triadPower, triad);
                resultStream << triadWords << separators[0] << powerName << separators[1];
            }
            else //last triad doesn't have name and separators
            {
                resultStream << triadWords;
            }
        }
    }
    
    return this->rtrim(resultStream.str());
}

std::string TriadNaturalConverter::rtrim(const std::string &str)
{
    const auto strEnd = str.find_last_not_of(' ');
    const auto strRange = strEnd + 1;
    
    return str.substr(0, strRange);
}

std::string TriadNaturalConverter::simpleConvertion(const std::string &pNumberAsString)
{
    if(pNumberAsString.length() > TRIAD_LENGTH)
    {
        return "";
    }
    
    Triad triad = std::stoi(pNumberAsString);
    
    if(triad == 0)
    {
        return this->getZeroName();
    }
    else
    {
        return this->triadToWords(triad, 0);
    }
}