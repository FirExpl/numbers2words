//
//  NumbersConvertionManager.hpp
//  Number2Words
//
//  Created by Artem Nikiforov on 06.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#ifndef NumbersConvertionManager_hpp
#define NumbersConvertionManager_hpp

#include <string>

class NumbersConvertionManager
{
private:
    static NumbersConvertionManager *s_instance;
    
protected:
    
    NumbersConvertionManager();
    
public:
    enum class Locale: int {
        EN_Gb = 0,
        LocalesCount
    };
    
    static NumbersConvertionManager* getInstance();
    
    std::string convertToWords(const std::string &pNumberAsString, Locale locale);
};

#endif /* NumbersConvertionManager_hpp */
