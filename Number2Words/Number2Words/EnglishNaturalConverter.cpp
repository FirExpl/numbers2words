//
//  EnglishNaturalConverter.cpp
//  Number2Words
//
//  Created by Artem Nikiforov on 08.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#include "EnglishNaturalConverter.hpp"
#include <sstream>

const std::vector<std::string> EnglishNaturalConverter::ONES_NAMES = {
    "",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
    "eleven",
    "twelve",
    "thirteen",
    "fourteen",
    "fifteen",
    "sixteen",
    "seventeen",
    "eighteen",
    "nineteen"
};

const std::vector<std::string> EnglishNaturalConverter::TENS_NAMES = {
    "twenty",
    "thirty",
    "forty",
    "fifty",
    "sixty",
    "seventy",
    "eighty",
    "ninety"
};

const std::vector<std::string> EnglishNaturalConverter::POWERS_NAMES = {
    "hundred",
    "thousand",
    "million",
    "billion",
    "trillion",
    "quadrillion",
    "quintillion",
    "sextillion",
    "septillion"
};

const std::vector<std::string> EnglishNaturalConverter::ONES_NAMES_SPECIAL = EnglishNaturalConverter::getSpecialOnesNames();

std::string toSpecial(const std::string &onesName)
{
    return "and " + onesName;
}

const std::vector<std::string> EnglishNaturalConverter::getSpecialOnesNames()
{
    std::vector<std::string> result(ONES_NAMES.size());
    std::transform(ONES_NAMES.begin(),
                   ONES_NAMES.end(),
                   result.begin(),
                   toSpecial);
    return result;
}

const int EnglishNaturalConverter::getMaxNumberLength()
{
    return (int)POWERS_NAMES.size() * TRIAD_LENGTH;
}

const std::vector<std::string>& EnglishNaturalConverter::getOnesNames(const Triad pTriad, int pTriadPower)
{
    if(pTriadPower == 0 &&
       pTriad < 100)
    {
        return ONES_NAMES_SPECIAL;
    }
    else
    {
        return ONES_NAMES;
    }
}

const std::vector<std::string>& EnglishNaturalConverter::getTensNames()
{
    return TENS_NAMES;
}

const std::string EnglishNaturalConverter::getPowerName(unsigned long pPowerNumber, const Triad pTriad)
{
    std::string result;
    
    if(pPowerNumber == 0)
    {
        int powerDigit = pTriad / 100;
        
        if(powerDigit > 0)
        {
            std::stringstream powerNameStream;
            powerNameStream << ONES_NAMES[powerDigit] << " " << POWERS_NAMES[0];
            
            result = powerNameStream.str();
        }
    }
    else
    {
        result = POWERS_NAMES[pPowerNumber];
    }
    
    return result;
}

const std::vector<std::string> EnglishNaturalConverter::getTriadInnerSeparators()
{
    return {" ", "-"};
}

const std::vector<std::string> EnglishNaturalConverter::getTriadsSeparators()
{
    return {" ", " "};
}

const std::string EnglishNaturalConverter::getZeroName()
{
    return "zero";
}

std::string EnglishNaturalConverter::simpleConvertion(const std::string &pNumberAsString)
{
    if(pNumberAsString.length() > TRIAD_LENGTH)
    {
        return "";
    }
    
    Triad triad = std::stoi(pNumberAsString);
    
    if(triad == 0)
    {
        return this->getZeroName();
    }
    else
    {
        return this->triadToWords(triad, 1);
    }
}

EnglishNaturalConverter::~EnglishNaturalConverter()
{
    
}