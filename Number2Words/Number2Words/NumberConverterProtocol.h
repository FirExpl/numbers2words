//
//  NumberConverterProtocol.h
//  Number2Words
//
//  Created by Artem Nikiforov on 07.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#ifndef NumberConverterProtocol_h
#define NumberConverterProtocol_h

#include <string>

class NumberConverterProtocol
{
public:
    virtual std::string convert(const std::string &pNumberStringRepresentation) = 0;
};

#endif /* NumberConverterProtocol_h */
