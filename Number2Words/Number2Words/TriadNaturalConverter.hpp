//
//  TriadNaturalConverter.hpp
//  Number2Words
//
//  Created by Artem Nikiforov on 07.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#ifndef TriadNaturalConverter_hpp
#define TriadNaturalConverter_hpp

#include "NumberConverterProtocol.h"
#include <vector>

class TriadNaturalConverter: public NumberConverterProtocol
{
protected:
    typedef short Triad;
    
    /* throw exception if input is invalid */
    virtual void validateInput(const std::string &pNumberAsString);
    
    /* get names for simple numbers usualy in range [0..19]
       pTriad - value of triad for names
       pTriadPower - number of triad, where 0 - the rightest triad
     */
    virtual const std::vector<std::string>& getOnesNames(const Triad pTriad, int pTriadPower) = 0;
    /* must return 8 names for 20, 30 ... 90 */
    virtual const std::vector<std::string>& getTensNames() = 0;
    /* return name for triad with given power
       pPowerNumber - number of triad, where 0 - the rightest triad (hundred triad)
                      example: in number 444333222111 the triad 444 has power 3, 111 has power 0
       pTriad - triad that have to have this name
     */
    virtual const std::string getPowerName(unsigned long pPowerNumber, const Triad pTriad) = 0;
    /* must return 2 values with separators of triad
       example: if triad convert to 3 words, it has structure word1 + sep0 + word2 + sep1 + word3
       example 2: if triad convert to 2 words, it has structure word1 + sep0 + word2
     */
    virtual const std::vector<std::string> getTriadInnerSeparators() = 0;
    /* must return 2 values with separators between triads and power names
       example: result have structure like triad2 + sep0 + powerName2 + sep1 + triad1 + sep0 + powerName1 + sep0 + triad0
     */
    virtual const std::vector<std::string> getTriadsSeparators() = 0;
    /* return word for 0 number */
    virtual const std::string getZeroName() = 0;
    /* separate given string to triads array */
    virtual std::vector<Triad> convertToTriads(const std::string &pNumberAsString);
    /* return MAX supported number length */
    virtual const int getMaxNumberLength() = 0;
    /* convert triad to it's words representation
       pTriad - triad value
       pTriadPower - number of triad, where 0 - the rightest triad
     */
    virtual std::string triadToWords(const Triad pTriad, int pTriadPower);
    /* convert triads array to them words representation
       pTriadsArray - array of triads in right order
     */
    virtual std::string joinTriadsAsWords(const std::vector<Triad> &pTriadsArray);
    /* convert one-triad number to it's words representation
       if function can't convert given input it must return empty string
       pNumberAsString - number's string representation
     */
    virtual std::string simpleConvertion(const std::string &pNumberAsString);
    /* remove space symbols from the end of string */
    virtual std::string rtrim(const std::string &str);
    
    virtual ~TriadNaturalConverter();
    
public:
    static const int TRIAD_LENGTH = 3;
    
    virtual std::string convert(const std::string &pNumberStringRepresentation) override;
};

#endif /* TriadNaturalConverter_hpp */
