//
//  EnglishNaturalConverter.hpp
//  Number2Words
//
//  Created by Artem Nikiforov on 08.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#ifndef EnglishNaturalConverter_hpp
#define EnglishNaturalConverter_hpp

#include "TriadNaturalConverter.hpp"

class EnglishNaturalConverter: public TriadNaturalConverter
{
protected:
    static const std::vector<std::string> getSpecialOnesNames();
    
    static const std::vector<std::string> ONES_NAMES;
    static const std::vector<std::string> ONES_NAMES_SPECIAL;
    static const std::vector<std::string> TENS_NAMES;

    static const std::vector<std::string> POWERS_NAMES;
    
    virtual const std::vector<std::string>& getOnesNames(const Triad pTriad, int pTriadPower) override;
    virtual const std::vector<std::string>& getTensNames() override;
    virtual const std::string getPowerName(unsigned long pPowerNumber, const Triad pTriad) override;
    virtual const std::vector<std::string> getTriadInnerSeparators() override;
    virtual const std::vector<std::string> getTriadsSeparators() override;
    virtual const std::string getZeroName() override;
    virtual const int getMaxNumberLength() override;
    
    virtual std::string simpleConvertion(const std::string &pNumberAsString) override;
    
public:
    ~EnglishNaturalConverter();
};

#endif /* EnglishNaturalConverter_hpp */
