//
//  NumbersTestTests.m
//  NumbersTestTests
//
//  Created by Artem Nikiforov on 09.04.16.
//  Copyright © 2016 FirExpl. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Number2Words/EnglishNaturalConverter.hpp"
#import <vector>

@interface NumbersTestTests : XCTestCase

@end

@implementation NumbersTestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRightValues {
    EnglishNaturalConverter converter;
    
    const std::vector<std::string> kNumbers = {
        "0",
        "3",
        "12",
        "45",
        "432",
        "9843",
        "23450",
        "682509",
        "934729835",
        "458634528342",
        "397893642929553",
        "749823462950532341",
        "1000000000000000000",
        "473926423498234723896",
        "987239486623492357896999",
        "687236484682764836486864786",
        "1322001"
    };
    
    const std::vector<std::string> kWords = {
        "zero",
        "three",
        "twelve",
        "forty-five",
        "four hundred thirty-two",
        "nine thousand eight hundred forty-three",
        "twenty-three thousand four hundred fifty",
        "six hundred eighty-two thousand five hundred nine",
        "nine hundred thirty-four million seven hundred twenty-nine thousand eight hundred thirty-five",
        "four hundred fifty-eight billion six hundred thirty-four million five hundred twenty-eight thousand three hundred forty-two",
        "three hundred ninety-seven trillion eight hundred ninety-three billion six hundred forty-two million nine hundred twenty-nine thousand five hundred fifty-three",
        "seven hundred forty-nine quadrillion eight hundred twenty-three trillion four hundred sixty-two billion nine hundred fifty million five hundred thirty-two thousand three hundred forty-one",
        "one quintillion",
        "four hundred seventy-three quintillion nine hundred twenty-six quadrillion four hundred twenty-three trillion four hundred ninety-eight billion two hundred thirty-four million seven hundred twenty-three thousand eight hundred ninety-six",
        "nine hundred eighty-seven sextillion two hundred thirty-nine quintillion four hundred eighty-six quadrillion six hundred twenty-three trillion four hundred ninety-two billion three hundred fifty-seven million eight hundred ninety-six thousand nine hundred ninety-nine",
        "six hundred eighty-seven septillion two hundred thirty-six sextillion four hundred eighty-four quintillion six hundred eighty-two quadrillion seven hundred sixty-four trillion eight hundred thirty-six billion four hundred eighty-six million eight hundred sixty-four thousand seven hundred eighty-six",
        "one million three hundred twenty-two thousand and one"
    };
    
    XCTAssertTrue(kNumbers.size() == kWords.size(), "Numbers count must be equal words count");
    
    for(int i = 0; i < kNumbers.size(); ++i)
    {
        try {
            auto result = converter.convert(kNumbers[i]);
            XCTAssertTrue(result == kWords[i], "%i test case failed: (%s)", i, result.c_str());
        }
        catch(...)
        {
            XCTFail("%i test case failed: Wrong input data!", i);
        }
    }
}

- (void)testWrongValues
{
    EnglishNaturalConverter converter;
    
    const std::vector<std::string> kNumbers = {
        "",
        "4323 439",
        " df324",
        "skdjhsdjkfh",
        "342893749.3475",
        "4729,42749",
        "68723648468276483648686478673",
        "4729734%3",
        "808304 ",
        "874925234_394"
    };
    
    for(int i = 0; i < kNumbers.size(); ++i)
    {
        try {
            auto result = converter.convert(kNumbers[i]);
            XCTFail("%i test case failed: Input mast be wrong!", i);
        }
        catch(...)
        {
        }
    }
}

@end
